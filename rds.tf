# Create RDS cluster...
resource "aws_rds_cluster" "awsRdsNotEncrypted" {
  master_password      = "test123"
  master_username      = "test"
}

# Create RDS cluster...
resource "aws_rds_cluster" "storageEncryptedNoKms" {
  master_password      = "barbarbarbar"
  master_username      = "foo"
  storage_encrypted    = true
}
